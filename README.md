ansible-kerberos-client
=======================

**ansible-kerberos-client** is an Ansible role to easily install a Kerberos Client.

Requirements
------------

In order to use this Ansible role, you will need:

* Ansible version >= 2.2 in your deployer machine.
* Check meta/main.yml if you need to check dependencies.

Main workflow
-------------

This role does:
* Download specific Kerberos packages (this packages are os-dependent).
* Configuring Kerberos Client files:
 * kdc.conf
 * kadm5.acl
 * krb5.conf

Role Variables
--------------

| Attribute 		| Default Value 	| Description  									|
|---        		|---				|---											|
| realm_name  		| REALM.NAME.COM	| Realm Name for Kerberos Server				|
| kdc_port  		| 88			  	| Kerberos Key Distribution Center (KDC) port 	|
| kdc_hostname  	| master  	| hostname of the kerberos master					  	|
